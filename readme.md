
### Description 

This is not a library, nor is it a proper project.

There is no build process and you will not be able to run this code without 
some work on your part.
If you want to use the code, copy it into your project and modify as necessary.


### Example Screen

The ExamplePaymentScreen.tsx is just just example code I copied and hacked 
up to not be specific to my project.  It likely won't run for you because it 
hasn't been tested - it's just an example of how to use the StripeCheckout 
component.


### Component code

The StripeCheckout component in the /stripe directory is lifted from my 
project directly - all I can say is that it seems to work for me.

Submit a PR if you have fixes you think should be included.


### Payment flow

The Stripe checkout library can be used in many different ways (tokenising 
card details, making charges immediately, etc.)
This component is designed only for obtaining a token that represents the 
user's credit card details that can then be used with the Stripe backend api 
for charging, etc.  In my project, I create a "Stripe Customer" for the current
user and attach the card (via the returned token) to the customer as the 
"default" payment source.  This can then be used later on to make payment 
charges.  




