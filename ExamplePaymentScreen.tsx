
import * as React from "react";
import {
  StripeHandler,
  StripeCheckoutToken
} from "./stripe/stripe-checkout";
import {StripeCheckout} from "./stripe/StripeCheckout";

interface PaymentScreenProps {
}
interface PaymentScreenState {
  processingCardOperation: boolean;
}

export class PaymentScreen
extends React.PureComponent<PaymentScreenProps, PaymentScreenState> {
  stripeHandler: StripeHandler;

  constructor(props: any, context: any){
    super(props, context);
    this.state = {
      loadingStripeLibrary: true,
    };
  }

  render(){
    return <div>
      <StripeCheckout
        dialogOptions={{
          apiPublicKey: 'pk_test_xxx',
          name: 'name',
          description: 'description',
          email: 'test@example.com',
          label: 'Set Card Details',
          image: 'https://example.com/site-logo.png',
          enableSimpleCcForm: false,
        }}
        handleStripeReturnedToken={this.handleStripeReturnedToken}
        handleStripeLibraryIsReady={(handler:StripeHandler)=>{
          this.setState({loadingStripeLibrary: false});
          this.stripeHandler = handler;
        }}
      />
      <button
        disabled={this.state.loadingStripeLibrary}
        onClick={this.props.handleSetCardClick}
      >Set Credit Card</button>
    </div>;
  }

  handleSetCardClick = (e: React.SyntheticEvent<any>)=>{
    console.log("handleSetCardClick() called", this.stripeHandler);

    this.stripeHandler.open();

    e.preventDefault();
    e.stopPropagation();
  };

  handleStripeReturnedToken = (token: StripeCheckoutToken)=>{
    // do something with the card token - send it to the server and attach to
    // a customer, charge a payment, etc.
  };

}