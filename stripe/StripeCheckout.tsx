import * as React from "react";
import {
  StripeApiHook,
  StripeCheckoutOptions, StripeCheckoutToken,
  StripeHandler
} from "./stripe-checkout";
import log4javascript = require("log4javascript");

const log = log4javascript.getLogger("StripeCheckout");

export interface StripeCheckoutProps {
  /** Simplified Stripe dialog options exposing only what we use */
  dialogOptions: {
    apiPublicKey: string;
    name: string;
    description: string;
    email: string;
    label: string;
    image: string;
    enableSimpleCcForm: boolean;
  };

  /**Called after Stripe processes and accepts the credit card details. */
  handleStripeReturnedToken: (token: StripeCheckoutToken)=>void;

  /** Called when the library is loaded and configured, you can then call
   * open()/close() on the handler.
   */
  handleStripeLibraryIsReady: (handler: StripeHandler)=>void;
}

interface StripeCheckoutState {
  libLoadState : "loading" | "configured";
}

/**
 * Encapsulates loading and using the Stripe checkout.js dynamically.
 * <p/>
 * The handleStripeLibraryIsReady(StripeHandler) function will be called once
 * checkout.js is loaded and configured, caller can then call open()/close()
 * on the StripeHandler argument passed.
 * <p/>
 * This component works by creating a new HTML head script element which it
 * loads with the Stripe checkout.js library (from stripe.com).
 * <p/>
 * Loading the Stripe code "just in time" this way is done to avoid
 * downloading and running the Stripe code if the user doesn't need it.
 * The Stripe code increases code size/document structure and it's bit noisy
 * (not to mention it was tracking users via MixPanel at one point) -
 * I don't want it running unless needed.
 */
export class StripeCheckout
extends React.PureComponent<StripeCheckoutProps, StripeCheckoutState> {
  private stripeHandler: StripeHandler;

  constructor(props: StripeCheckoutProps, context: any){
    super(props, context);
    this.state = {
      libLoadState: "loading"
    };
  }

  /**
   * loadStripe() is called from didMount() because we're storing the handler
   * in a class member.
   * When StripeCheckout goes away (screen change, etc.) it gets GC'd,
   * so member field value is lost.
   * Also, as per random SO question that I started this code from:
   * https://stackoverflow.com/questions/41500135/react-js-stripe-checkout-is-not-working/
   * <p/>
   * We're calling close() on the StripeHandler in willUnmount(),
   * so I'm assuming that does something that would cause configure() to need
   * to be called again.
   * <p/>
   * Could probably store the handler in a global or up on the AppState
   * somwhere, but it feels like it might be a good thing to do it this way?
   * No idea, really.
   */
  componentDidMount(){
    if( window.StripeCheckout ){
      log.debug("Stripe checkout.js already loaded");
      this.configureStripeHandler(window.StripeCheckout);
      this.props.handleStripeLibraryIsReady(this.stripeHandler);
    }
    else {
      log.debug("loading Stripe checkout.js into document head");
      this.createScriptElement(
        'https://checkout.stripe.com/checkout.js',
        () =>{
          // window.StripeCheckout is assigned by the checkout.js script itself,
          // should be set by the time this is called
          log.debug("Stripe script loaded");
          this.configureStripeHandler(window.StripeCheckout);
          this.props.handleStripeLibraryIsReady(this.stripeHandler);
        } );
    }
  };

  componentWillUnmount(){
    if( this.stripeHandler ){
      this.stripeHandler.close();
    }
  }

  render(){
    return null;
  }

  private configureStripeHandler(checkoutRef: StripeApiHook){
    this.stripeHandler = checkoutRef.configure(
      this.createStripeCheckoutOptions()
    );
    this.setState({libLoadState: "configured"});
    log.debug("this.stripeHandler = Stripe.configure()", this.stripeHandler);
  }

  private createScriptElement(
    sourceUrl: string,
    onloadCallback: ()=>void,
  ){
    const script = document.createElement('script');
    script.onload = ()=>{
      onloadCallback();
    };
    script.src = sourceUrl;
    document.head.appendChild(script);
  };

  private createStripeCheckoutOptions(): StripeCheckoutOptions {
    let dialogOptions:StripeCheckoutOptions = {
      key: this.props.dialogOptions.apiPublicKey,
      name: this.props.dialogOptions.name,
      description: this.props.dialogOptions.description,
      email: this.props.dialogOptions.email,
      panelLabel: this.props.dialogOptions.label,
      image: this.props.dialogOptions.image,
      locale: 'auto',

      // enables a remember-me checkbox that wants a phone number?
      // skip it, since we're keeping the CC at stripe anyway
      allowRememberMe: false,

      token: this.props.handleStripeReturnedToken,

      // should probably be tracking / doing state management around this?
      // I once had a significant pause between clicking the react button
      // and having the dialog show up (haven't been able to repro).
      opened: ()=>{ log.debug("stripe dialog opened") },
      closed: ()=>{ log.debug("stripe dialog closed") },
    };

    // better fraud prevention, simple-cc-form is used only for testing
    if( !this.props.dialogOptions.enableSimpleCcForm ){
      dialogOptions.zipCode = true;
      dialogOptions.billingAddress = true;
    }

    return dialogOptions
  }

}

