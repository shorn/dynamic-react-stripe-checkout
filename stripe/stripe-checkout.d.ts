/**
 * See https://stripe.com/docs/checkout#integration-custom
 *
 * These typings are not complete by any means.  They're just the bare minimum
 * I required to use the checkout library for my use-case
 * (create card token that can be attached to a Stripe customer).
 */

/** The checkout.js script defines this global window variable as
 * it's loaded.
 */
declare global {
  interface Window {
    StripeCheckout: StripeApiHook;
  }
}

/** Once the library has been loaded, you can then call
 * window.StripeCheckout.configure() to initialise the library.
 */
export interface StripeApiHook{
  configure(options?: StripeCheckoutOptions) : StripeHandler;
}

/** The configure() method returns a StripeHandler object that you can then
 * call open() / close() on.
 */
export interface StripeHandler {
  close(): void;
  open(options?: StripeCheckoutOptions): void;
}

/**
 * From https://stripe.com/docs/checkout#integration-custom
 * Only API definition I could find.
 * These options can be given to the configure() or open() methods.
 */
export interface StripeCheckoutOptions {
  /** Stripe pusblishable account key.
   * Will look like: "pk_test_XXX" or "pk_live_XXX".
   */
  key?: string;

  email?: string;

  /** Can also be specified in dialog open options */
  name?: string;

  /** Can also be specified in dialog open options */
  description?: string;

  /** URL pointing to a square image of your brand/product icon */
  image?: string;

  /** The button text. Default is "Pay". */
  panelLabel?: string;

  locale?: "auto";

  allowRememberMe?: boolean;

  /** Can also be specified in dialog open options */
  zipCode?: boolean;

  /** Can also be specified in dialog open options */
  billingAddress?: boolean;

  /** Callback invoked with new token when checkout process is complete.
   * Appears that this doesn't get called if user cancels dialog.
   */
  token?: (token: StripeCheckoutToken) => void;

  currency?: string;
  amount?: number,

  opened?: ()=>void;
  closed?: ()=>void;
}

/** https://stripe.com/docs/api#tokens */
export interface StripeCheckoutToken {
  /** This is the value that is used to add the card to the customer.
   * I don't know if it's always present, I'm just assuming.
   */
  id: string;
  card: StripeCardDetails;
}

export type DataCheck = "pass" | "fail" | "unavailable" | "unchecked";

export interface StripeCardDetails{
  id: string;
  object: string;

  address_city?: string;
  address_country?: string;
  address_line1?: string;
  address_line1_check?: DataCheck;
  address_line2: string;
  address_state: string;
  address_zip: string;
  address_zip_check?: DataCheck;

  funding: string;
  brand: "Visa" | "American Express" | "MasterCard" |
    "Discover" | "JCB" | "Diners Club" | "Unknown";
  country?: string;

  cvc_check: DataCheck;

  exp_month: number;
  exp_year: number;

  last4: string;
  name: string;
}

/*
StripeCheckoutToken real result of a test call with the standard 4242
test card.
{
  "id": "tok_XXX",
  "object": "token",
  "card": {
    "id": "card_XXX",
    "object": "card",
    "address_city": null,
    "address_country": null,
    "address_line1": null,
    "address_line1_check": null,
    "address_line2": null,
    "address_state": null,
    "address_zip": null,
    "address_zip_check": null,
    "brand": "Visa",
    "country": "US",
    "cvc_check": "pass",
    "dynamic_last4": null,
    "exp_month": 1,
    "exp_year": 2020,
    "funding": "credit",
    "last4": "4242",
    "metadata": {
    },
    "name": "text@example.com",
    "tokenization_method": null
  },
  "client_ip": "1.2.3.4",
  "created": 1517546709,
  "email": "test@example.com",
  "livemode": false,
  "type": "card",
  "used": false
}
*/